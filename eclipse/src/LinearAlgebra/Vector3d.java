//Vashti Lanz Rubio
//1931765
package LinearAlgebra;

public class Vector3d {
	private double x;
	private double y;
	private double z;
	
	public Vector3d(double x, double y, double z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}
	public double getX() {
		return x;
	}
	public double getY() {
		return y;
	}
	public double getZ() {
		return z;
	}
	public double magnitude() {
		return Math.sqrt(Math.pow(x,2) + Math.pow(y,2) + Math.pow(z,2));
	}
	public double dotProduct(Vector3d v) {
		return (x * v.getX()) + (y * v.getY()) + (z * v.getZ());
	}
	public Vector3d add(Vector3d v) {
		return new Vector3d(x + v.getX(), y + v.getY(), z + v.getZ());
	}
	//Testing
	/*
	public static void main(String[] args) {
		Vector3d v1 = new Vector3d(1,2.5,3);
		Vector3d v2 = new Vector3d(4,5,6.1);
		System.out.println(v1.getX());
		System.out.println(v1.getY());
		System.out.println(v1.getZ());
		System.out.println(v1.magnitude());
		System.out.println(v1.dotProduct(v2));

		Vector3d v3 = v1.add(v2);
		System.out.println(v3.getX());
		System.out.println(v3.getY());
		System.out.println(v3.getZ());
		
		Vector3d v5 = new Vector3d(1,2.5,-3);
		System.out.println(v5.magnitude());
	}*/
}
