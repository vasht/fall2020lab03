//Vashti Lanz Rubio
//1931765
package LinearAlgebra;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class Vector3dTests {

	@Test
	void testGetMethods() {
		Vector3d v = new Vector3d(1,2.5,-3);
		double actualX = v.getX();
		double actualY = v.getY();
		double actualZ = v.getZ();
		
		double expectedX = 1;
		double expectedY = 2.5;
		double expectedZ = -3;
		
		assertEquals(expectedX, actualX);
		assertEquals(expectedY, actualY);
		assertEquals(expectedZ, actualZ);
	}
	@Test
	void testMagnitude() {
		Vector3d v = new Vector3d(1,2.5,-3);
		double actual =  v.magnitude();
		double expected = 4.031128874149275;
		assertEquals(expected,actual,0.000000000000001);
	}
	@Test
	void testDotProduct() {
		Vector3d v1 = new Vector3d(1,2.5,-3);
		Vector3d v2 = new Vector3d(4,5,6);	
		double actual = v1.dotProduct(v2);
		double expected = -1.5;
		assertEquals(expected,actual);
	}
	@Test
	void testAdd() {
		Vector3d v1 = new Vector3d(1,2.5,-3);
		Vector3d v2 = new Vector3d(4,5,6);	
		
		Vector3d actual = v1.add(v2);
		Vector3d expected = new Vector3d(5,7.5,3);
		
		assertEquals(expected.getX(),actual.getX());
		assertEquals(expected.getY(),actual.getY());
		assertEquals(expected.getZ(),actual.getZ());
		
	}
}
